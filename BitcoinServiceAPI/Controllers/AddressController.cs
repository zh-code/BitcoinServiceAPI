using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace BitcoinServiceAPI.Controllers{
    [Route("Address")]
    public class AddressController : Controller
    {
        /// <summary>
        /// Get New Bitcoin Address
        /// </summary>
        /// <param name="API">API Key</param>
        /// <returns>String</returns>
        [SwaggerResponse(200, null, "Ok")]
        [SwaggerResponse(403, null, "Forbidden")]
        [SwaggerResponse(500, null, "Server Error")]
        [HttpGet("getnewaddress")]
        public async Task<IActionResult> getnewaddress(string API)
        {
            if (await StaticVars.IsUser(API))
            {
                using (HttpResponseMessage response = await StaticVars.Client.GetAsync("https://btc.source.zh-code.com/getnewaddress.php"))
                {
                    string address = await response.Content.ReadAsStringAsync();
                    if (!String.IsNullOrEmpty(address) && await AddPublicKey(API, address))
                    {
                        return Ok(address);
                    }
                    else
                    {
                        return StatusCode(500);
                    }
                }
            }
            return StatusCode(403);
        }

        /// <summary>
        /// Get Private Key
        /// </summary>
        /// <param name="API">API Key</param>
        /// <param name="address">Public address got from previous API</param>
        /// <returns></returns>
        [SwaggerResponse(200, null, "Ok")]
        [SwaggerResponse(403, null, "Forbidden")]
        [SwaggerResponse(500, null, "Server Error")]
        [HttpGet("dumpprivkey")]
        public async Task<IActionResult> dumpprivkey(string API, string Address)
        {
            if (await StaticVars.IsUser(API))
            {
                using (HttpResponseMessage response = await StaticVars.Client.GetAsync("https://btc.source.zh-code.com/dumpprivkey.php?address=" + Address))
                {
                    string privKey = await response.Content.ReadAsStringAsync();
                    if (!String.IsNullOrEmpty(privKey))
                    {
                        try
                        {
                            UpdatePrivKey(API, Address, privKey);
                        }
                        catch {}
                        return Ok(privKey);
                    }
                    return StatusCode(500);
                }
            }
            return StatusCode(403);
        }

        private async Task<bool> AddPublicKey(string API, string PublicKey)
        {
            using (HttpResponseMessage response = await StaticVars.Client.PostAsync("https://api.zh-code.com/v1/bitcoin/addpublickey?api=" + API + 
            "&publicKey=" + PublicKey, null))
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
            }
            return false;
        }

        private void UpdatePrivKey(string API, string PublicKey, string PrivKey)
        {
            StaticVars.Client.PostAsync("https://api.zh-code.com/v1/bitcoin/updateprivatekey?api=" + API + 
            "&publicKey=" + PublicKey + "&privateKey=" + PrivKey, null);
        }
    }
}