using System.Net.Http;
using System.Threading.Tasks;

namespace BitcoinServiceAPI.Controllers
{
    public class StaticVars
    {
        public static HttpClient Client = new HttpClient();

        public static async Task<bool> IsUser(string API)
        {
            using (HttpResponseMessage response = await Client.GetAsync("https://api.zh-code.com/v1/user/check?api=" + API))
            {
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        return bool.Parse(await response.Content.ReadAsStringAsync());
                    }
                    catch {}
                }
            }
            return false;
        }
    }

}